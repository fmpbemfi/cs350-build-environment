FROM ubuntu:12.04

ADD tarballs/ /root/tarballs/
ADD sys161.conf /root/sys161.conf

ADD scripts/setup.sh /root/scripts/setup.sh
RUN /root/scripts/setup.sh

ADD scripts/run.sh /root/run

VOLUME /root/cs350-os161/
