.PHONY: run

tarballs/os161-binutils.tar.gz:
	wget http://www.student.cs.uwaterloo.ca/~cs350/os161_repository/os161-binutils.tar.gz -P tarballs

tarballs/os161-gcc.tar.gz:
	wget http://www.student.cs.uwaterloo.ca/~cs350/os161_repository/os161-gcc.tar.gz -P tarballs

tarballs/os161-gdb.tar.gz:
	wget http://www.student.cs.uwaterloo.ca/~cs350/os161_repository/os161-gdb.tar.gz -P tarballs

tarballs/os161-bmake.tar.gz:
	wget http://www.student.cs.uwaterloo.ca/~cs350/os161_repository/os161-bmake.tar.gz -P tarballs

tarballs/os161-mk.tar.gz:
	wget http://www.student.cs.uwaterloo.ca/~cs350/os161_repository/os161-mk.tar.gz -P tarballs

tarballs/sys161.tar.gz:
	wget http://www.student.cs.uwaterloo.ca/~cs350/os161_repository/sys161.tar.gz -P tarballs

tarballs/os161.tar.gz:
	wget http://www.student.cs.uwaterloo.ca/~cs350/os161_repository/os161.tar.gz -P tarballs

cs350-os161: tarballs/os161.tar.gz
	mkdir -p cs350-os161
	tar -xzf tarballs/os161.tar.gz
	mv os161-1.99 cs350-os161/

.make.dockerimage: tarballs/os161-binutils.tar.gz tarballs/os161-gcc.tar.gz tarballs/os161-gdb.tar.gz tarballs/os161-bmake.tar.gz tarballs/os161-mk.tar.gz tarballs/sys161.tar.gz
	docker build . -t cs350-build-environment
	touch .make.dockerimage

current_dir = $(shell pwd)

run: .make.dockerimage cs350-os161
	docker run -v "$(current_dir)/cs350-os161:/root/cs350-os161" -it cs350-build-environment
