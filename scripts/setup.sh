#!/bin/bash

set -euo pipefail

# DEPENDENCIES
apt-get update
apt-get install -y gcc make texinfo tmux libncurses5-dev
apt-get clean
rm -rf /var/lib/apt/lists/

# BINUTILS
cd "$HOME/tarballs/"
tar -xzf os161-binutils.tar.gz
cd binutils-2.17+os161-2.0.1/
./configure --nfp --disable-werror --target=mips-harvard-os161 --prefix="$HOME/sys161/tools"
make
make install
mkdir -p "$HOME/sys161/bin"
export PATH=${PATH}:${HOME}/sys161/bin:${HOME}/sys161/tools/bin

# GCC
cd "$HOME/tarballs/"
tar -xzf os161-gcc.tar.gz
cd gcc-4.1.2+os161-2.0/
./configure -nfp --disable-shared --disable-threads --disable-libmudflap --disable-libssp --target=mips-harvard-os161 --prefix="$HOME/sys161/tools"
make
make install

# GDB
cd "$HOME/tarballs/"
tar -xzf os161-gdb.tar.gz
cd gdb-6.6+os161-2.0
./configure --target=mips-harvard-os161 --prefix="$HOME/sys161/tools" --disable-werror
make
make install

# BMake
cd "$HOME/tarballs/"
tar -xzf os161-bmake.tar.gz
cd bmake
tar -xzf ../os161-mk.tar.gz 
./boot-strap --prefix="$HOME/sys161/tools"
mkdir -p "$HOME/sys161/tools/bin"
cp "$HOME/tarballs/bmake/Linux/bmake" "$HOME/sys161/tools/bin/bmake-20101215"
rm -f "$HOME/sys161/tools/bin/bmake"
ln -s bmake-20101215 "$HOME/sys161/tools/bin/bmake"
mkdir -p "$HOME/sys161/tools/share/man/cat1"
cp "$HOME/tarballs/bmake/bmake.cat1" "$HOME/sys161/tools/share/man/cat1/bmake.1"
sh "$HOME/tarballs/bmake/mk/install-mk" "$HOME/sys161/tools/share/mk"

# SYS161 prep
mkdir -p "$HOME/sys161/bin"
cd "$HOME/sys161/tools/bin"
# sh -c 'for i in mips-*; do ln -s $HOME/sys161/tools/bin/$i $HOME/sys161/bin/cs350-`echo $i | cut -d- -f4-`; done'
for i in mips-*; do
    ln -s "$HOME/sys161/tools/bin/$i" "$HOME/sys161/bin/cs350-$(echo "$i" | cut -d- -f4-)"
done
ln -s "$HOME/sys161/tools/bin/bmake" "$HOME/sys161/bin/bmake"

# SYS161
cd "$HOME/tarballs/"
tar -xzf sys161.tar.gz
cd sys161-1.99.06
./configure --prefix="$HOME/sys161" mipseb
make
make install

# BASHRC
echo '# MODIFCATIONS' >> "$HOME/.bashrc"
echo 'export PATH=${PATH}:$HOME/sys161/bin:$HOME/sys161/tools/bin' >> "$HOME/.bashrc"
echo 'export MAKESYSPATH=$HOME/sys161/tools/share/mk/' >> "$HOME/.bashrc"

# TMUXCONF
echo 'unbind C-b' >> "$HOME/.tmux.conf"
echo 'set -g prefix C-a' >> "$HOME/.tmux.conf"
