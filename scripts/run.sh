#!/bin/bash

set -euxo pipefail

if [ -z "$1" ]; then
    echo "Usage:"
    echo "  ./run <assignment number> [--userspace] [--debug]"
    echo ""
    echo "Examples: './run 0' will execute ASST0"
    echo "          './run 0 --userspace' will compile the userspace"
    echo "          programs and execute ASST0"
    echo "          './run 0 --debug' will execute ASST0 with gdb"
    echo ""
    echo "NOTE: the tmux control prefix in the debugging session is ctrl-a."
    exit 0
fi

cd "$HOME/cs350-os161/os161-1.99"
./configure --ostree="$HOME/cs350-os161/root" --toolprefix=cs350-

if echo "$@" | grep "userspace"; then
    cd "$HOME/cs350-os161/os161-1.99/"
    bmake
    bmake install
fi

cd "$HOME/cs350-os161/os161-1.99/kern/conf"
./config "ASST${1}"

cd "$HOME/cs350-os161/os161-1.99/kern/compile/ASST${1}/"
bmake depend
bmake
bmake install

ln -sfn "$HOME/sys161.conf" "$HOME/cs350-os161/root/sys161.conf"

cd "$HOME/cs350-os161/root/"
if echo "$@" | grep "debug"; then
    tmux new-session -d bash
    tmux split-window -h bash
    tmux send -t 0:0.0 "sys161 -w kernel" C-m
    tmux send -t 0:0.1 "cs350-gdb kernel" C-m
    tmux send -t 0:0.1 "dir ../os161-1.99/kern/compile/ASST0" C-m
    sleep 0.5
    tmux send -t 0:0.1 "target remote unix:.sockets/gdb" C-m
    tmux -2 attach-session -d
else
    sys161 kernel
fi
