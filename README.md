# CS350 OS161 Docker Build Environment

To use, make sure that docker is installed, the docker daemon is running, and your user is a member of the `docker` group.

Run by invoking `make run`.
The initial build will take a long time (5-10 minutes), but it only needs to build the first time you run it.
When it finishes, you'll be dumped into a bash session inside the container.

From that shell, run `/root/run 0` to compile & execute ASST0. `/root/run 1` will do ASST1, and so forth.

Run `/root/run 0 --debug` to compile & execute ASST0 in debug mode.
Note: this will create a tmux session where the prefix is ctrl-a instead of the usual ctrl-b.
This way, it won't interfere with any tmux sessions you are using outside of the container.

Run `/root/run 0 --userspace` to compile ASST0 and all of the userspace programs, and then execute ASST0.

The makefile will initialize a fresh copy of the os161 code at cs350-os151/os161-1.99.
Replace this with your own copy of the code.

The cs350-os161 folder is shared between the container and the host.
Changes you make to your code will immediately be reflected inside of a live container.

If, for whatever reason, you want to rebuild your container, remove `.make.dockerimage` and then call `make run`.

## Troubleshooting

`Unable to find image 'cs350-build-environment:latest' locally`: Remove the file `.make.dockerimage` and make again.

`error checking context: 'can't stat 'cs350-build-environment/cs350-os161/root/.sockets''`: Remove the `root/.sockets` folder.
